<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>

<body>

    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="get">
        <label for="first-name">First name :</label><br><br>
        <input type="text" name="first-name">
        <br><br>
        <label for="last-name">Last name :</label><br><br>
        <input type="text" name="last-name">
        <br><br>
        <label for="gender">Gender :</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other
        <br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="american">American</option>
            <option value="indian">Indian</option>
        </select>
        <br><br>
        <label for="lang">Language Spoken :</label><br><br>
        <input type="checkbox" name="indonesia">Bahasa indonesia <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="other">Other
        <br><br>
        <label for="bio">Bio :</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>